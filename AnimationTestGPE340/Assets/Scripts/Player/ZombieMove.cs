﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZombieMove : Pawn
{ 
    // Declaring the variables
    
    public Camera mainCamera; 
    public Transform cursorObject;
    public GameObject meleeIcon;
    public GameObject rifleIcon;
    public GameObject pistolIcon;
    public float popUpTime = 5f; 

    // Update function including crouch settings
    void Update()
    {
        // Calling the movement functions
        Rotation();
        Movement();
    }

    // Rotation function
    void Rotation()
    {
        // Create the plane for the cursor object
        Plane thePlane = new Plane(Vector3.up, tf.position);

        // Finding the distance of the raycast
        float distance;
        Ray theRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        thePlane.Raycast(theRay, out distance);

        // Setting a point on the ray as the target
        Vector3 lookPoint = theRay.GetPoint(distance);

        // Setting the empty gameobject or "cursor" to the position 
        cursorObject.position = lookPoint;

        // Rotating the character to look at this point
        RotateTowards(lookPoint);
    }

    // Movement function
    void Movement()
    {
        // Using the inputs to move the character
        Vector3 moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
        moveDirection = Vector3.ClampMagnitude(moveDirection, 1.0f);
        moveDirection = tf.InverseTransformDirection(moveDirection);

        // Calling the pawn move function
       Move(moveDirection);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Bullet")
        {
            Health.Damage(); 
        }

        if (other.gameObject.tag == "AK")
        {
            Debug.Log("Rifle");
            rifleIcon.SetActive(true);

            Invoke("InactivePopUp", popUpTime);
        }

        if(other.gameObject.tag == "Handgun")
        {
            Debug.Log("Handgun");
            pistolIcon.SetActive(true);

            Invoke("InactivePopUp", popUpTime);
        }

        if (other.gameObject.tag == "Melee")
        {
            Debug.Log("Melee");
            meleeIcon.SetActive(true);

            Invoke("InactivePopUp", popUpTime);
        }
    }

    void InactivePopUp()
    {
        rifleIcon.SetActive(false);
        pistolIcon.SetActive(false);
        meleeIcon.SetActive(false);
    }
}
