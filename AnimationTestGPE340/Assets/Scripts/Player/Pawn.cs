﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class Pawn : MonoBehaviour
{
    // Declaring the variables
    public static Pawn player;
    public RagdollController ragdoll;
    public Animator anim;
    [HideInInspector]public Transform tf;
    [HideInInspector]public Health Health { get; private set; }
    [HideInInspector]public GameManager gm;

    [Header("Movement Settings")]
    [SerializeField] private float turnSpeed = 10f;
    public float moveSpeed = 10f;

    [Header("Weapon Settings")]
    public Weapon equippedWeapon; 
    public Transform weaponPoint;

    // Use this for initialization
    void Start()
    {
        player = this;
        anim = GetComponent<Animator>();
        tf = GetComponent<Transform>();
        Health = GetComponent<Health>();
    }

    // Sets the character's movement direction inputs
    public void Move(Vector3 direction)
    {
        // Setting the direction 
        anim.SetFloat("Vertical", direction.z * moveSpeed);
        anim.SetFloat("Horizontal", direction.x * moveSpeed);
    }

    // Rotate towards function
    public void RotateTowards(Vector3 lookPoint)
    {
        // Having the character rotate to face the desired point
        Vector3 vectorToLookDown = lookPoint - tf.position;
        Quaternion lookRotation = Quaternion.LookRotation(vectorToLookDown, tf.up);
        tf.rotation = Quaternion.RotateTowards(tf.rotation, lookRotation, turnSpeed * Time.deltaTime);
    }

    // Setting the animation type
    void SetAnimType()
    {
        anim.SetInteger("Weapon", gm.weapon.weaponNum);
    }

    // Setting up the character's animator
    protected virtual void OnAnimatorIK()
    {
        if (!equippedWeapon)
        {
            return;
        }

        // If there is a RHPoint on the weapon
        if (equippedWeapon.RHPoint)
        {
            anim.SetIKPosition(AvatarIKGoal.RightHand, equippedWeapon.RHPoint.position);
            anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 1f);
            anim.SetIKRotation(AvatarIKGoal.RightHand, equippedWeapon.RHPoint.rotation);
            anim.SetIKRotationWeight(AvatarIKGoal.RightHand, 1f);
        }
        else
        {
            anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 0f);
            anim.SetIKRotationWeight(AvatarIKGoal.RightHand, 0f);
        }

        // If there is a LHPoint on the weapon
        if (equippedWeapon.LHPoint)
        {
            anim.SetIKPosition(AvatarIKGoal.LeftHand, equippedWeapon.LHPoint.position);
            anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1f);
            anim.SetIKRotation(AvatarIKGoal.LeftHand, equippedWeapon.LHPoint.rotation);
            anim.SetIKRotationWeight(AvatarIKGoal.LeftHand, 1f);
        }
        else
        {
            anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, 0f);
            anim.SetIKRotationWeight(AvatarIKGoal.LeftHand, 0f);
        }
    }

}
