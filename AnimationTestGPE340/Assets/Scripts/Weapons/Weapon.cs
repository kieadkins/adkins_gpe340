﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class Weapon : PickUp
{ 
    [Header("Weapon Settings")]
    public Transform LHPoint;
    public Transform RHPoint;
    protected Transform tf;
    public Transform barrel;
    public int amountOfAmmunition = 100;
    public int weaponNum;
    public bool activeToShoot = true;
    [HideInInspector] public Shoot shoot;

    public AudioSource audioSource;
    public AudioClip gunShot;

    private void Awake()
    {
        tf = GetComponent<Transform>();
    }

    public WeaponAnimationType weaponType;

    public enum WeaponAnimationType
    {
        None,
        Rifle,
        Handgun,
        Grenade,
        Axe
    }

    public virtual void DetermineWeapon()
    {
        Debug.Log("Determine weapon");
    }
}
