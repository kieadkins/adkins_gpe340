﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour {

    // Declaring the variables
    private Weapon weapon;
    public float muzzleVelocity;
    public Projectile projectile;

    private void Awake()
    {
        weapon = GetComponent<Weapon>();       
    }

    void Update()
    {

        if (weapon.amountOfAmmunition != 0)
        {
            weapon.activeToShoot = true;
        }
        else
        {
            weapon.activeToShoot = false;
        }

        // Capturing user input
        if (weapon.activeToShoot == true && Input.GetButtonDown("Fire"))
        {
            weapon.amountOfAmmunition -= 1;

            if (weapon.audioSource)
            {
                weapon.audioSource.PlayOneShot(weapon.gunShot);
            }

            // Instantiating the new bullet
            Rigidbody shellInstance = Instantiate(projectile.rb, weapon.barrel.position, weapon.barrel.rotation) as Rigidbody;

            // Adding force to the bullet
            shellInstance.velocity = muzzleVelocity * weapon.barrel.forward; ;
            
        }
    }
}
