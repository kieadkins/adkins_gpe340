﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    // Declaring the variables
    public float damage;
    public float lifeSpan;
    public Rigidbody rb;
    public GameManager gm; 

    // Initializing the game object
    private void Start()
    {
        rb = GetComponent<Rigidbody>();

        Destroy(gameObject, lifeSpan * Time.deltaTime);
    }
}
