﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Target : MonoBehaviour {

    // Declaring the variables
    public static Target target; 
    private UnityEvent onDie;
    [SerializeField]private float respawnTime = 15f;

    void Start()
    {
        target = this;
    }

    // On trigger enter
    private void OnTriggerEnter(Collider other)
    {
        // Making the object disappear and calling it to reappear
        gameObject.SetActive(false);
        //GameManager.gm.killCount++;
        Invoke("Activate", respawnTime);
    }

    // Setting the target active again
    void Activate()
    {
        gameObject.SetActive(true);
    }
   
    // On object death
    void OnDeath()
    {
        //onDie.Invoke();
    }
}
