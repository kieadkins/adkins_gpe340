﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShoot : MonoBehaviour
{
    // Declaring the variables
    public Pawn player;
    public float targetDistance = 25f;
    public EnemyController enemy; 
    private Weapon weapon;
    public float bulletsPerMinute = 6f; 
    public float muzzleVelocity;
    public Projectile projectile;
    public bool triggerPulled = false;
    public float timeNextShotIsReady;

    public AudioSource audioSource;
    public AudioClip gunShot;

    private void Awake()
    {
        weapon = GetComponent<Weapon>();
    }

    void Update()
    {
        if (enemy.playerDistance <= targetDistance)
        {
            weapon.activeToShoot = true;
            Attack();
        }

        if (triggerPulled)
        {
            while (Time.time > timeNextShotIsReady)
            {
                InvokeRepeating("Shoot", 0f, bulletsPerMinute);
                timeNextShotIsReady += 60f / bulletsPerMinute;
            }
        }
        else if (Time.time > timeNextShotIsReady)
        {
            timeNextShotIsReady = Time.time;
        }
    }

    /// <summary>
    ///  Attack Function
    /// </summary>
    void Attack()
    {

        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit))
        {

            if (weapon.activeToShoot == true && hit.collider.gameObject.tag == "Player")
            {
                Debug.Log("Raycast hit");

                triggerPulled = true;

            }
        }
    }

    /// <summary>
    /// Shoot Function
    /// </summary>
    void Shoot()
    {
        if (audioSource)
        {
            audioSource.PlayOneShot(gunShot);
        }

        // Instantiating the new bullet
        Rigidbody shellInstance = Instantiate(projectile.rb, weapon.barrel.position, weapon.barrel.rotation) as Rigidbody;

        // Adding force to the bullet
        shellInstance.velocity = muzzleVelocity * weapon.barrel.forward;
    }
}
