﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RagdollController : MonoBehaviour
{
    public Collider mainCD;
    public Rigidbody mainRB;
    public Animator anim;
    public NavMeshAgent agent; 

    [Header("Respawn Controls")]
    public EnemyController enemy;
    public float spawnTime = 5f;
    public Transform[] spawnPoints;
    public float destroyTime; 

    public List<Rigidbody> partRB;
    public List<Collider> partCD; 

    // Start is called before the first frame update
    void Start()
    {
        mainCD = GetComponent<Collider>();
        mainRB = GetComponent<Rigidbody>();

        partRB = new List<Rigidbody>(GetComponentsInChildren<Rigidbody>());
        partCD = new List<Collider>(GetComponentsInChildren<Collider>());

        DeactivateRagdoll();
    }

    public void ActivateRagdoll()
    {
        // Turn on all the child rigidbodies
        foreach (Rigidbody rb in partRB)
        {
            rb.isKinematic = false;
        }
        // Turn on all the child coliders 
        foreach (Collider col in partCD)
        {
            col.enabled = true;
        }
        // Turn OFF the main stuff
        mainCD.enabled = false;
        mainRB.isKinematic = true;
        anim.enabled = false;
        agent.enabled = false;
        Destroy(gameObject, destroyTime);

        Spawn(); 
    }

    void Spawn()
    {
        // Find a random index between zero and one less than the number of spawn points.
        int spawnPointIndex = Random.Range(0, spawnPoints.Length);

        // Create an instance of the enemy prefab at the randomly selected spawn point's position and rotation.
        Instantiate(enemy, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
    }


    public void DeactivateRagdoll()
    {
        // Turn OFF the ragdoll colliders
        foreach (Collider col in partCD)
        {
            col.enabled = false;
        }
        // Turn OFF the ragdoll rigidbodies
        foreach (Rigidbody rb in partRB)
        {
            rb.isKinematic = true;
        }
        // Turn ON the main stuff
        mainCD.enabled = true;
        mainRB.isKinematic = false;
        agent.enabled = true;
        anim.enabled = true;
    }
}
