﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI; 

public class EnemyController : Pawn
{
    public NavMeshAgent agent;
    public GameObject target;
    public float chaseStart;
    public float playerDistance;
    public GameObject deathPopUp;
    public float popUpTime = 2f;
    private float eMaxHealth = 100f; 
    public float eCurrentHealth = 100f;
    private float eMinHealth = 0f;
    public float eDamage = 10f;
    public Image enemyHealthRadial;

    void OnAwake()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.SetDestination(target.transform.position);
        eCurrentHealth = eMaxHealth; 
    }

    // Update is called once per frame
    void Update()
    {
        EnemyHealthUI();

        playerDistance = Vector3.Distance(target.transform.position, transform.position);

        if (playerDistance < chaseStart)
        {
            if (agent.enabled == true)
            {
                Vector3 desiredVelocity = agent.desiredVelocity * moveSpeed;
                Vector3 enemyMove = Vector3.MoveTowards(desiredVelocity, agent.desiredVelocity, agent.acceleration * Time.deltaTime);
                enemyMove = transform.InverseTransformDirection(enemyMove);
                anim.SetFloat("Horizontal", enemyMove.x * Time.deltaTime);
                anim.SetFloat("Vertical", enemyMove.z * Time.deltaTime);
                agent.SetDestination(target.transform.position);
            }
        }
    }

    private void OnAnimatorMove()
    {
        agent.velocity = anim.velocity;
    }

    /// <summary>
    /// When hit with a bullet
    /// </summary>
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Bullet")
        {
            Health(); 

            if (eCurrentHealth == 0)
            {
                OnDeath(); 
            }
        }
    }

    /// <summary>
    /// Death Function
    /// </summary>
    void OnDeath()
    {
        Debug.Log("Death");

        DeathPopUp(); 

        GameManager.gm.killCount++;

        eCurrentHealth = 100;

        equippedWeapon.activeToShoot = false;

        ragdoll.ActivateRagdoll();
    }

    /// <summary>
    ///  Enabling Death Pop Up
    /// </summary>
    void DeathPopUp()
    {
        deathPopUp.SetActive(true);

        Invoke("InactivePopUp", popUpTime);
    }

    /// <summary>
    ///  Disabling Death Pop Up
    /// </summary>
    void InactivePopUp()
    {
        deathPopUp.SetActive(false);
    }

    // AI damage clamping
    void Health()
    {
            Debug.Log("Hit");
            eDamage = Mathf.Max(eDamage, 10f);
            eCurrentHealth = Mathf.Clamp(eCurrentHealth - eDamage, eMinHealth, eMaxHealth);
    }

    // AI Health UI function
    void EnemyHealthUI()
    {
        enemyHealthRadial.fillAmount = eCurrentHealth / 100;
    }

}
