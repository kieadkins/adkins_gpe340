﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using System.Linq;

public class GameManager : MonoBehaviour {

    // Declaring the variables
    public static GameManager gm;

    [Header("UI Information")]
    public Pawn player;
    public Health health;
    public Weapon weapon;
    private Target target;
    public EnemyController enemy;

    public int killCount = 0;
    public Text killText;
    public Image healthRadial;
    public Image ammoRadial;
    public GameObject winScreen;
    public GameObject lossScreen;
    public Slider musicVolume;
    public Dropdown resolutionDropdown;
    public Dropdown qualityDropdown;
    public Toggle fullscreenToggle; 

    [Header("Audio Information")]
    [SerializeField]private AnimationCurve volumeDecibels;
    public AudioMixer audioMixer;
    
    // Use this for initialization
    void Awake()
    {
        gm = this;

        //Build resolutions
        resolutionDropdown.ClearOptions();
        List<string> resolutions = new List<string>();
        for (int index = 0; index < Screen.resolutions.Length; index++)
        {
            resolutions.Add(string.Format("{0} x {1}", Screen.resolutions[index].width, Screen.resolutions[index].height));
        }
        resolutionDropdown.AddOptions(resolutions);
        // Build quality levels
        qualityDropdown.ClearOptions();
        qualityDropdown.AddOptions(QualitySettings.names.ToList());
    }

    private void OnEnable()
    {
        musicVolume.value = PlayerPrefs.GetFloat("MusicVolume", musicVolume.maxValue);
        fullscreenToggle.isOn = Screen.fullScreen;
        qualityDropdown.value = QualitySettings.GetQualityLevel();
    }

    // On update
    private void Update()
    {
        // Controlling the volume
        audioMixer.SetFloat("MasterVolume", volumeDecibels.Evaluate(musicVolume.value));
     
        // Setting all radial bars
        SetKillText();
        HealthUI();
        AmmoUI();

        if (killCount == 3)
        {
            Win();
        }

        // If health gets to zero
        if (health.currentHealth == 0)
        {
            health.onDeath();
        }
    }

    // Setting the kill count text
    void SetKillText()
    {
        killText.text = killCount.ToString (); 
    }

    // Setting the health UI bar
    void HealthUI()
    {
        healthRadial.fillAmount = health.currentHealth / 100;
    }

    void AmmoUI()
    {
        if (player.equippedWeapon != null)
        {
            ammoRadial.fillAmount = player.equippedWeapon.amountOfAmmunition / 100;
        }
    }

    // Setting the win popup freeze
    void Win()
    {
        winScreen.SetActive(true);
        Time.timeScale = 0f;
    }

    // Setting the loss popup freeze
    public void Loss()
    {
        lossScreen.SetActive(true);
        Time.timeScale = 0f;
    }
}
