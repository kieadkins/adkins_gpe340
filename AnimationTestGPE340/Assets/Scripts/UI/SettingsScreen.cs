﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class SettingsScreen : MonoBehaviour
{
    public Slider musicVolume;
    public Dropdown resolutionDropdown;
    public Dropdown qualityDropdown;
    public Toggle fullscreenToggle;

    // Start is called before the first frame update
    void Start()
    {
        // Build resolutions
        resolutionDropdown.ClearOptions();
        List<string> resolutions = new List<string>();
        for (int index = 0; index < Screen.resolutions.Length; index++)
        {
            resolutions.Add(string.Format("{0} x {1}", Screen.resolutions[index].width, Screen.resolutions[index].height));
        }
        resolutionDropdown.AddOptions(resolutions);

        // Build quality levels
        qualityDropdown.ClearOptions();
        qualityDropdown.AddOptions(QualitySettings.names.ToList());
    }

    private void OnEnable()
    {
        musicVolume.value = PlayerPrefs.GetFloat("MusicVolume", musicVolume.maxValue);
        fullscreenToggle.isOn = Screen.fullScreen;
        qualityDropdown.value = QualitySettings.GetQualityLevel();
    }
}
