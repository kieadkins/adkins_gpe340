﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadPopUp : MonoBehaviour
{
    public GameObject popUp;

    public static bool popUpActive = false;

    // Update is called once per frame
    public void loadPopUp(bool popUpActive)
    {

         if (popUpActive == false)
         {
            popUp.SetActive(false);
         }
    
        else
        {
            popUp.SetActive(true);
        }
    }
}
