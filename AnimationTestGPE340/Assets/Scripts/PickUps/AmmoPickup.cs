﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoPickup : PickUp
{
    public int ammunitionResupply;
    public Pawn player; 
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            OnPickUp();
        }
    }

    protected override void OnPickUp()
    {
        player.equippedWeapon.amountOfAmmunition += ammunitionResupply;
        base.OnPickUp();
    }
}
