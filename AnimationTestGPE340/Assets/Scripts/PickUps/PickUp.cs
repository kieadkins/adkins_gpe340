﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PickUp : MonoBehaviour {

    public GameManager gm;
    public float waitTime;
    public GameObject pickUpItem;

    float timer;

    // On pickup function
    protected virtual void OnPickUp()
    {
        gameObject.SetActive(false);

       Invoke("Instance", waitTime);
    }

    void Instance()
    {
        gameObject.SetActive(true);
    }
}

