﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponPickup : PickUp {
    public Pawn player;
    public Weapon weaponModel;
    [SerializeField] public Transform weaponPoint;
    private Weapon Weapon;
    private Pawn equippedWeapon;

    private void OnTriggerEnter(Collider other)
    {
        // Determining the weapon type
        weaponModel.DetermineWeapon();
        OnPickUp();
    }

    // Equipping the weapon
    public void EquipWeapon(Weapon weaponModel)
    {
        player.equippedWeapon = Instantiate(weaponModel, transform.position, transform.rotation) as Weapon;
        player.equippedWeapon.transform.SetParent(weaponPoint);
        player.equippedWeapon.transform.localPosition = weaponModel.transform.localPosition;
        player.equippedWeapon.transform.localRotation = weaponModel.transform.localRotation;
    }

    // Unequipping the weapon
    public void UnEquipWeapon(Pawn equippedWeapon)
    {
        // Unequipping weapon if already equipped
        if (player.equippedWeapon)
        {
            Destroy(player.equippedWeapon.gameObject);
            player.equippedWeapon = null;
        }

        // No weapon equipped
        else
        {
            Debug.Log("No weapon equipped");
        }
    }

    // On pickup of weapon
    protected override void OnPickUp()
    {
        UnEquipWeapon(equippedWeapon);
        EquipWeapon(weaponModel);

        base.OnPickUp();
    }
}
