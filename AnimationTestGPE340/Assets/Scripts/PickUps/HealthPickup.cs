﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickup : PickUp {
    
    public float healAmount = 10f;
    public Transform tf;
    [SerializeField] private float spinSpeed = 100f;

    private void Start()
    {
        tf = GetComponent<Transform>();
    }

    private void Update()
    {
        tf.Rotate(Vector3.up, spinSpeed * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" && gm.health.currentHealth < 100f)
        {
            OnPickUp();
        }
    }

    protected override void OnPickUp()
    {
        gm.health.Heal(healAmount);

        base.OnPickUp();
    }
}

