﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour {

    public float healthMax = 100f;
    public float damage = 10f;
    public float currentHealth = 0f;
    [SerializeField] public float healthMin = 0f;

    public GameManager gm;
    private DamagePickup pickUp;

    // Use this for initialization
    void Awake () {
        // Set current as health at the start of the game 
        currentHealth = healthMax;
    }

    public void Heal(float healAmount)
    {
        Debug.Log("Health");
        healAmount = Mathf.Max(healAmount, 0f);
        currentHealth = Mathf.Clamp(currentHealth + healAmount, healthMin, healthMax);
        gm.healthRadial.fillAmount = gm.health.currentHealth;
    }

    public void Damage()
    {
        Debug.Log("Hit");
        damage = Mathf.Max(damage, 10f);
        currentHealth = Mathf.Clamp(currentHealth - damage, healthMin, healthMax);
    }

    public void onDeath()
    {
        gm.Loss();
    }
}
